var startingMoney, money; //starting money
var rolls, maxMoney, rollsAtMaxMoney; //info holders
var a, b; //used as dice rolls

function play(){
    startingMoney = document.getElementById("Bet").value;
    document.getElementById("Bet").value = "$0.00";
    money = startingMoney;
    rolls = 0;
    maxMoney = money;
    rollsAtMaxMoney = 1;

    while(money > 0){
        rolls += 1;
        a = Math.floor(Math.random() * 6) + 1;
        b = Math.floor(Math.random() * 6) + 1;

        if(a+b == 7){
            money += 4;
        }
        else{
            money -= 1;
        }

        if(money > maxMoney) {
            maxMoney = money;
            rollsAtMaxMoney = rolls;
        }
    }
    document.getElementById("Results").innerHTML = "" +
        "<div style='background-color: ghostwhite; width:220px;'>" +
        "   <h3>Results</h3>" +
        "   <table style='width:200px; background-color: ghostwhite; border: solid ghostwhite 1px' align='center' border='1'>" +
        "       <tr style='background-color: lightgray'><th>Starting Bet</th><th>$" + startingMoney + ".00</th></tr>" +
        "       <tr><td>Total Rolls Before Going Broke</td><td>" + rolls + "</td></tr>" +
        "       <tr><td>Highest Amount Won</td><td>$" + maxMoney + ".00</td></tr>" +
        "       <tr><td>Roll Count at Highest Amount Won</td><td>" + rollsAtMaxMoney +"</td></tr>" +
        "   </table>" +
        "</div>"
    document.getElementById("Play").innerHTML = "Play Again";

}